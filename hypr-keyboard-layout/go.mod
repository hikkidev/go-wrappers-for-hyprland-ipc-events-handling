module hypr-keyboard-layout

go 1.19

require (
	hypr-ipc-client v1.0.0
)

replace (
	hypr-ipc-client v1.0.0 => ../hypr-ipc-client
)
