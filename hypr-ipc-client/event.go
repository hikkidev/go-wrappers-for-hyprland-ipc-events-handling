package hypr_ipc_client

import (
	"strconv"
	"strings"
)

func Subscribe(c IPC, ev EventHandler, events ...EventType) error {
	for {
		msg, err := c.Receive()
		if err != nil {
			return err
		}

		for _, data := range msg {
			processEvent(ev, data, events)
		}
	}
}

func processEvent(ev EventHandler, msg ReceivedData, events []EventType) {
	for _, event := range events {
		raw := strings.Split(msg.Data, ",")
		if msg.Type == event {
			switch event {
			case EventWorkspace:
				num, err := strconv.Atoi(raw[0])
				if err != nil {
					panic(err)
				}
				ev.Workspace(WorkspaceId(num))
				break
			case EventFocusedMonitor:
				num, err := strconv.Atoi(raw[1])
				if err != nil {
					panic(err)
				}
				ev.FocusedMonitor(FocusedMonitor{
					MonitorName: MonitorName(raw[0]),
					WorkspaceId: WorkspaceId(num),
				})
				break
			case EventActiveWindow:
				// e.g. jetbrains-goland,hyprland-ipc-ipc – main.go
				ev.ActiveWindow(ActiveWindow{
					Name:  raw[0],
					Title: raw[1],
				})

				break
			case EventFullscreen:
				// e.g. "true" or "false"
				ev.Fullscreen(raw[0] == "1")
				break
			case EventMonitorRemoved:
				// e.g. idk
				ev.MonitorRemoved(MonitorName(raw[0]))
				break
			case EventMonitorAdded:
				// e.g. idk
				ev.MonitorAdded(MonitorName(raw[0]))
				break
			case EventCreateWorkspace:
				num, err := strconv.Atoi(raw[0])
				if err != nil {
					panic(err)
				}
				ev.CreateWorkspace(WorkspaceId(num))
				break
			case EventDestroyWorkspace:
				num, err := strconv.Atoi(raw[0])
				if err != nil {
					panic(err)
				}
				ev.DestroyWorkspace(WorkspaceId(num))
				break
			case EventMoveWorkspace:
				num, err := strconv.Atoi(raw[0])
				if err != nil {
					panic(err)
				}
				ev.MoveWorkspace(MoveWorkspace{
					WorkspaceId: WorkspaceId(num),
					MonitorName: MonitorName(raw[1]),
				})
				break
			case EventActiveLayout:
				// e.g. AT Translated Set 2 keyboard,Russian
				ev.ActiveLayout(ActiveLayout{
					Type: raw[0],
					Name: raw[1],
				})
				break
			case EventOpenWindow:
				num, err := strconv.Atoi(raw[1])
				if err != nil {
					panic(err)
				}
				ev.OpenWindow(OpenWindow{
					Address:     raw[0],
					WorkspaceId: WorkspaceId(num),
					Class:       raw[2],
					Title:       raw[3],
				})
				break
			case EventCloseWindow:
				// e.g. 5
				ev.CloseWindow(CloseWindow{
					Address: raw[0],
				})
				break
			case EventMoveWindow:
				num, err := strconv.Atoi(raw[1])
				if err != nil {
					panic(err)
				}
				ev.MoveWindow(MoveWindow{
					Address:     raw[0],
					WorkspaceId: WorkspaceId(num),
				})
				break
			case EventOpenLayer:
				// e.g. wofi
				ev.OpenLayer(OpenLayer(raw[0]))
				break
			case EventCloseLayer:
				// e.g. wofi
				ev.CloseLayer(CloseLayer(raw[0]))
				break
			case EventSubMap:
				// e.g. idk
				ev.SubMap(SubMap(raw[0]))
				break
			}

		}
	}
}
