package hypr_ipc_client

type DefaultIPC struct{}

func (d DefaultIPC) Receive() ([]ReceivedData, error) {
	return []ReceivedData{}, nil
}

func (d DefaultIPC) Dispatch(Args) ([]byte, error) {
	return []byte{}, nil
}

func (d DefaultIPC) Workspaces() ([]Workspace, error) {
	return []Workspace{}, nil
}

func (d DefaultIPC) Monitors() ([]Monitor, error) {
	return []Monitor{}, nil
}

func (d DefaultIPC) Clients() ([]Client, error) {
	return []Client{}, nil
}

func (d DefaultIPC) ActiveWindow() (Window, error) {
	return Window{}, nil
}

func (d DefaultIPC) Layers() (Layers, error) {
	return Layers{}, nil
}

func (d DefaultIPC) Devices() (Devices, error) {
	return Devices{}, nil
}

func (d DefaultIPC) Version() (Version, error) {
	return Version{}, nil
}

func (d DefaultIPC) Keyword(Args) error {
	return nil
}

func (d DefaultIPC) Reload() error {
	return nil
}

func (d DefaultIPC) SetCursor(string, string) error {
	return nil
}

func (d DefaultIPC) Kill() error {
	return nil
}

func (d DefaultIPC) Splash() (string, error) {
	return "", nil
}

func (d DefaultIPC) GetOption(string) (string, error) {
	return "", nil
}

func (d DefaultIPC) CursorPos() (CursorPos, error) {
	return CursorPos{}, nil
}
