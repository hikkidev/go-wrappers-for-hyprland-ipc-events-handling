package hypr_ipc_client

type DefaultEvHandler struct{}

func (e *DefaultEvHandler) Workspace(WorkspaceId)         {}
func (e *DefaultEvHandler) FocusedMonitor(FocusedMonitor) {}
func (e *DefaultEvHandler) ActiveWindow(ActiveWindow)     {}
func (e *DefaultEvHandler) Fullscreen(bool)               {}
func (e *DefaultEvHandler) MonitorRemoved(MonitorName)    {}
func (e *DefaultEvHandler) MonitorAdded(MonitorName)      {}
func (e *DefaultEvHandler) CreateWorkspace(WorkspaceId)   {}
func (e *DefaultEvHandler) DestroyWorkspace(WorkspaceId)  {}
func (e *DefaultEvHandler) MoveWorkspace(MoveWorkspace)   {}
func (e *DefaultEvHandler) ActiveLayout(ActiveLayout)     {}
func (e *DefaultEvHandler) OpenWindow(OpenWindow)         {}
func (e *DefaultEvHandler) CloseWindow(CloseWindow)       {}
func (e *DefaultEvHandler) MoveWindow(MoveWindow)         {}
func (e *DefaultEvHandler) OpenLayer(OpenLayer)           {}
func (e *DefaultEvHandler) CloseLayer(CloseLayer)         {}
func (e *DefaultEvHandler) SubMap(SubMap)                 {}
