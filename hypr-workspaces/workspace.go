package main

import (
	"encoding/json"
	client "hypr-ipc-client"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

// region Types ---------------------
type PersistantWorkspace struct {
	Name    string `json:"name"`
	Startup string `json:"startup"`
}

type Workspace struct {
	Id           client.WorkspaceId `json:"id"`
	Name         string             `json:"name"`
	Startup      string             `json:"startup"`
	IsOccupied   bool               `json:"isOccupied"`
	IsPersistent bool               `json:"isPersistent"`
	Windows      []string           `json:"-"`
}

type Display struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type WidgetState struct {
	FocusedWorkspaceId client.WorkspaceId `json:"focusedWorkspaceId"`
	Workspaces         []Workspace        `json:"workspaces"`
	Display            Display            `json:"display"`
}

type EventHandler struct {
	State WidgetState
	client.DefaultEvHandler
}

func (msg *PersistantWorkspace) UnmarshalJSON(text []byte) error {
	type Alias PersistantWorkspace
	aux := Alias{
		Startup: "",
	}
	if err := json.Unmarshal(text, &aux); err != nil {
		return err
	}
	*msg = PersistantWorkspace(aux)
	return nil
}

func (workspace *Workspace) updateOccupiedState() bool {
	isOccupied := len(workspace.Windows) > 0

	isUpdated := workspace.IsOccupied != isOccupied
	workspace.IsOccupied = isOccupied

	return isUpdated
}

// endregion Types ---------------------

// region Slice Utils ---------------------
func ContainsElement[T comparable](slice []T, elem T) bool {
	for _, item := range slice {
		if item == elem {
			return true
		}
	}
	return false
}

func Contains[T any](slice []T, matchFunc func(T) bool) bool {
	return FindIndex(slice, matchFunc) != -1
}

func Filter(ws []client.Workspace, monitorName string) []client.Workspace {
	filtered := make([]client.Workspace, 0)
	for _, w := range ws {
		if w.MonitorName == monitorName {
			filtered = append(filtered, w)
		}
	}
	return filtered
}

func FindIndex[T any](slice []T, matchFunc func(T) bool) int {
	for index, element := range slice {
		if matchFunc(element) {
			return index
		}
	}
	return -1
}

func RemoveElementByIndex[T any](slice []T, index int) []T {
	sliceLen := len(slice)
	sliceLastIndex := sliceLen - 1

	if index != sliceLastIndex {
		slice[index] = slice[sliceLastIndex]
	}

	return slice[:sliceLastIndex]
}

func FindFirst(arr []client.Monitor, name string) *client.Monitor {
	for _, n := range arr {
		if n.Name == name {
			return &n
		}
	}
	return nil
}

// endregion Slice Utils ---------------------

// region Callback ---------------------

func (e *EventHandler) MoveWorkspace(data client.MoveWorkspace) {
	//TODO
}

func (handler *EventHandler) Workspace(id client.WorkspaceId) {
	handler.State.FocusedWorkspaceId = id
	RenderState(&handler.State)
}

func (handler *EventHandler) CreateWorkspace(id client.WorkspaceId) {
	idx := FindIndex(handler.State.Workspaces, func(workspace Workspace) bool {
		return workspace.Id == id
	})
	if idx == -1 {
		mw := Workspace{
			Id:           id,
			Name:         strconv.Itoa(int(id)),
			Startup:      "",
			IsOccupied:   false,
			IsPersistent: false,
		}
		handler.State.Workspaces = append(handler.State.Workspaces, mw)
		RenderState(&handler.State)
	}
}

func (handler *EventHandler) DestroyWorkspace(id client.WorkspaceId) {
	idx := FindIndex(handler.State.Workspaces, func(workspace Workspace) bool {
		return workspace.Id == id
	})

	if idx != -1 && !handler.State.Workspaces[idx].IsPersistent {
		handler.State.Workspaces = RemoveElementByIndex(handler.State.Workspaces, idx)
		RenderState(&handler.State)
	}
}

func (handler *EventHandler) OpenWindow(data client.OpenWindow) {

	workspaceIdx := FindIndex(handler.State.Workspaces, func(workspace Workspace) bool {
		return workspace.Id == data.WorkspaceId
	})

	if !ContainsElement(handler.State.Workspaces[workspaceIdx].Windows, data.Address) {
		handler.State.Workspaces[workspaceIdx].Windows = append(handler.State.Workspaces[workspaceIdx].Windows, data.Address)
		if handler.State.Workspaces[workspaceIdx].updateOccupiedState() {
			RenderState(&handler.State)
		}
	}
}

func (handler *EventHandler) CloseWindow(data client.CloseWindow) {
	comparable := func(windowName string) bool {
		return windowName == data.Address
	}

	workspaceIdx := FindIndex(handler.State.Workspaces, func(workspace Workspace) bool {
		return workspace.Id == handler.State.FocusedWorkspaceId
	})
	windowsIdx := FindIndex(handler.State.Workspaces[workspaceIdx].Windows, comparable)
	if windowsIdx == -1 {
		for wIndex, workspace := range handler.State.Workspaces {
			windowsIdx = FindIndex(workspace.Windows, comparable)
			if windowsIdx != -1 {
				workspaceIdx = wIndex
				break
			}
		}
	}

	if windowsIdx != -1 {
		handler.State.Workspaces[workspaceIdx].Windows = RemoveElementByIndex(handler.State.Workspaces[workspaceIdx].Windows, windowsIdx)
		if handler.State.Workspaces[workspaceIdx].updateOccupiedState() {
			RenderState(&handler.State)
		}
	}
}

func (handler *EventHandler) MoveWindow(data client.MoveWindow) {
	needsUpdate := false
	comparable := func(windowName string) bool {
		return windowName == data.Address
	}

	//Delete
	workspaceIdx := FindIndex(handler.State.Workspaces, func(workspace Workspace) bool {
		return workspace.Id == handler.State.FocusedWorkspaceId
	})
	windowIdx := FindIndex(handler.State.Workspaces[workspaceIdx].Windows, comparable)
	if windowIdx == -1 {
		for wIndex, workspace := range handler.State.Workspaces {
			windowIdx = FindIndex(workspace.Windows, comparable)
			if windowIdx != -1 {
				workspaceIdx = wIndex
				break
			}
		}
	}
	if windowIdx != -1 {
		handler.State.Workspaces[workspaceIdx].Windows = RemoveElementByIndex(handler.State.Workspaces[workspaceIdx].Windows, windowIdx)
		needsUpdate = handler.State.Workspaces[workspaceIdx].updateOccupiedState()
	}

	//Append
	workspaceIdx = FindIndex(handler.State.Workspaces, func(workspace Workspace) bool {
		return workspace.Id == data.WorkspaceId
	})
	if !ContainsElement(handler.State.Workspaces[workspaceIdx].Windows, data.Address) {
		handler.State.Workspaces[workspaceIdx].Windows = append(handler.State.Workspaces[workspaceIdx].Windows, data.Address)
		needsUpdate = handler.State.Workspaces[workspaceIdx].updateOccupiedState() || needsUpdate
	}

	if needsUpdate {
		RenderState(&handler.State)
	}
}

// endregion Callback ---------------------

func RenderState(state *WidgetState) {
	sort.Slice(state.Workspaces, func(i, j int) bool {
		return state.Workspaces[i].Id < state.Workspaces[j].Id
	})
	enc := json.NewEncoder(os.Stdout)
	if err := enc.Encode(&state); err != nil {
		log.Println(err)
	}
}

func Json2Std(s any) {
	if err := json.NewEncoder(os.Stdout).Encode(s); err != nil {
		log.Fatal(err)
	}
}

// region Input
func Map2MWorkspace(pw []PersistantWorkspace, ow []client.Workspace, windows []client.Client) []Workspace {
	out := []Workspace{}
	for index, w := range pw {
		mw := Workspace{
			Id:           client.WorkspaceId(index + 1),
			Name:         w.Name,
			Startup:      w.Startup,
			IsOccupied:   false,
			IsPersistent: true,
		}
		out = append(out, mw)
	}

	for _, w := range ow {
		wId := client.WorkspaceId(w.Id)
		idx := FindIndex(out, func(workspace Workspace) bool {
			return workspace.Id == wId
		})
		alreadyContainsWorkspace := idx != -1

		windowNames := []string{}
		for _, client := range windows {
			if client.Workspace.Id == w.Id {
				windowNames = append(windowNames, strings.TrimPrefix(client.Address, "0x"))
			}
		}
		if alreadyContainsWorkspace {
			out[idx].IsOccupied = w.Windows > 0
			out[idx].Windows = windowNames
		} else {
			mw := Workspace{
				Id:           wId,
				Name:         w.Name,
				Startup:      "",
				IsOccupied:   w.Windows > 0,
				IsPersistent: false,
				Windows:      windowNames,
			}
			out = append(out, mw)
		}
	}
	sort.Slice(out, func(i, j int) bool {
		return out[i].Id < out[j].Id
	})
	return out
}

func readConfig(cname string) ([]PersistantWorkspace, error) {
	defaultWs := &[]PersistantWorkspace{}
	jsonFile, err := os.Open(cname)
	if err != nil {
		return *defaultWs, err
	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	if err := json.Unmarshal([]byte(byteValue), &defaultWs); err != nil {
		return *defaultWs, err
	}
	return *defaultWs, nil
}

// endregion Input

func main() {
	if len(os.Args) < 2 {
		log.Println("Please provide display name!")
		os.Exit(-1)
	}
	configPath := ""
	if len(os.Args) > 2 {
		configPath = os.Args[2]
	}
	displayName := os.Args[1]

	c := client.NewClient(os.Getenv("HYPRLAND_INSTANCE_SIGNATURE"))

	allOccupiedWorkspaces, _ := c.Workspaces()
	monitors, _ := c.Monitors()
	if !Contains(monitors, func(monitor client.Monitor) bool { return monitor.Name == displayName }) {
		return
	}
	persistantWorkspaces, _ := readConfig(configPath)

	monitor := FindFirst(monitors, displayName)
	windows, _ := c.Clients()
	occupiedWorkspaces := Map2MWorkspace(persistantWorkspaces, Filter(allOccupiedWorkspaces, displayName), windows)

	initialState := WidgetState{
		FocusedWorkspaceId: client.WorkspaceId(monitor.ActiveWorkspace.Id),
		Workspaces:         occupiedWorkspaces,
		Display: Display{
			Id:   monitor.Id,
			Name: monitor.Name,
		},
	}
	RenderState(&initialState)
	client.Subscribe(c, &EventHandler{State: initialState}, client.EventWorkspace, client.EventCreateWorkspace, client.EventDestroyWorkspace, client.EventOpenWindow, client.EventCloseWindow, client.EventMoveWindow)
}
